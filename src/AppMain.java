
public class AppMain {
	public static void main(String[] args) {
		
		Person person1 = new Programmer("Jaja","Hilir","Java");
		Person person2 = new Teacher("Joko","Tegal","Matematika");
		Person person3 = new Doctor("Eko","Surabaya","Pedistrician");
		
		System.out.println(((Programmer)person1).tecknology);
		System.out.println();
		
		sayHello(person1);
		sayHello(person2);
		sayHello(person3);
		
	}
	static void sayHello(Person person) {
		String msg;
		if(person instanceof Programmer) {
			Programmer programmer = (Programmer) person;
			msg = "Hello,"+programmer.name+". Seorang programmer "+programmer.tecknology+".";
		}else if(person instanceof Doctor) {
			Doctor doctor = (Doctor) person;
			msg = "Hello,"+doctor.name+". Seorang doctor "+doctor.specialist+".";
		}else if(person instanceof Teacher) {
			Teacher teacher = (Teacher) person;
			msg = "Hello,"+teacher.name+". Seorang teacher "+teacher.subject+".";
		}else {
			msg = "Helo, "+person.name+".";
		}
		System.out.println(msg);
	}
}
